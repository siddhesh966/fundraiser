import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, Validators } from '../../../node_modules/@angular/forms';
import { SharedProvider } from '../../providers/shared/shared';

/**
 * Generated class for the DonationFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-donation-filter',
  templateUrl: 'donation-filter.html',
})
export class DonationFilterPage {

  public filterForm: any;
  public donations: any;
  public filteredResult: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController,
  private formBuilder: FormBuilder, private sharedProvider: SharedProvider) {
    this.donations = navParams.get('donations')
    console.log(this.donations)
    this.filterForm = this.formBuilder.group({
      year: ['', Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DonationFilterPage');
  }

  doFilter(){
    let date = this.filterForm.controls['year'].value
    this.filteredResult = this.donations.filter(donation => (donation.post_date <= (parseInt(date)+1)+'-04-01' && donation.post_date >= date+'-03-31'))
    console.log(this.filteredResult)
    this.dismiss()
  }

  dismiss(){
    this.viewCtrl.dismiss(this.filteredResult)
  }

}

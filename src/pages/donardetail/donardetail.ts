import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { DonationProvider } from '../../providers/donation/donation';
import { SocialSharing } from '../../../node_modules/@ionic-native/social-sharing';

/**
 * Generated class for the DonardetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-donardetail',
  templateUrl: 'donardetail.html',
})
export class DonardetailPage {

  donationId: any
  data: any
  token: any
  donarDetail: any

  constructor(public navCtrl: NavController, public navParams: NavParams,private viewCtrl: ViewController,
    private sharedProvider: SharedProvider, private donationProvider: DonationProvider, private socialSharing: SocialSharing) {
      this.token = localStorage.getItem('token')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DonardetailPage');
  }

  enquireViaMail(email){
    let subject = 'UnitedWay Mumbai'
    this.socialSharing.shareViaEmail('', subject, email)
  }

  ionViewWillEnter(){
    this.donationId = this.navParams.get('donationId')
    this.data = {
      'donationId': this.donationId
    };
    this.sharedProvider.showLoader()
    this.donationProvider.getDonarDetail(this.token, this.data).then(result=>{
      this.sharedProvider.dismissLoader()
      this.donarDetail = result
    }).catch(err=>{
      this.sharedProvider.dismissLoader()
      console.log(err)
    })
  }

  dismiss(){
    this.viewCtrl.dismiss()
  }

}

import { Component } from '@angular/core';
import { NavController, NavParams, Events, MenuController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { HomePage } from '../home/home';
import { ForgotpasswordPage } from '../forgotpassword/forgotpassword';
import { RegisterPage } from '../register/register';
import { UserProvider } from '../../providers/user/user';
import { SharedProvider } from '../../providers/shared/shared';
import { CampaignSelectorPage } from '../campaign-selector/campaign-selector';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;
  loginResponse: any;
  token: any;
  userResponse:any;
  tokenData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,
    private userProvider: UserProvider,private sharedProvider: SharedProvider,private event: Events,private menu: MenuController) {
      this.menu.enable(false)
      this.loginForm = this.formBuilder.group({
        email: ['', Validators.required],
        password: ['', Validators.required]
      }) 
  }

  doLogin(){
    this.sharedProvider.showLoader()
    this.userProvider.authenticate(this.loginForm.value).then(result=>{
      this.loginResponse = result
      console.log(this.loginResponse)
      if(this.loginResponse.token){
        localStorage.setItem('token', this.loginResponse.token)
        this.updateDeviceToken(this.loginResponse.token)
        this.fetchUserProfile(this.loginResponse.token)
      }else{
        this.sharedProvider.dismissLoader()
        this.sharedProvider.presentToast(this.loginResponse.message)  
      }
    }).catch(err=>{
      this.sharedProvider.dismissLoader()
      this.sharedProvider.presentToast("Invalid username and password.")
      console.log(JSON.stringify(err))
    })
  }

  updateDeviceToken(token){
    this.tokenData = {
      'deviceId': localStorage.getItem('tokenId')
    }

    this.userProvider.updateDevice(token, this.tokenData).then(result=>{
      console.log(result)
    }).catch(err=>{
      console.log(err) 
    })
  }

  fetchUserProfile(token){
    this.token = token
    console.log(this.token)
    this.userProvider.detail(this.token).then(result=>{
      this.sharedProvider.dismissLoader()
      this.userResponse = result
      console.log(this.userResponse)
      localStorage.setItem('userId',this.userResponse.user_id)
      localStorage.setItem('fullName', this.userResponse.name)
      localStorage.setItem('email', this.userResponse.email)
      localStorage.setItem('image', this.userResponse.img_file)
      this.event.publish('user:updated',[])
      let campaign = localStorage.getItem('campaign')
      if(campaign!=null){
        this.navCtrl.setRoot(HomePage)
      }else{
        this.navCtrl.setRoot(CampaignSelectorPage)
      }
    }).catch(err=>{
      this.sharedProvider.dismissLoader()
      this.sharedProvider.presentToast("Invalid username and password.")
      console.log(err)
    })
  }

  openRegistrationForm(){
    this.navCtrl.push(RegisterPage)
  }

  openForgotPasswordForm(){
    this.navCtrl.push(ForgotpasswordPage)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}

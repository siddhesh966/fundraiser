import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { DonardetailPage } from '../donardetail/donardetail';
import { SharedProvider } from '../../providers/shared/shared';
import { DonationProvider } from '../../providers/donation/donation';
import { DonationFilterPage } from '../donation-filter/donation-filter';

/**
 * Generated class for the DonationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-donation',
  templateUrl: 'donation.html',
})
export class DonationPage {

  token: any;
  donations: any;
  data: any;
  tmpDonations: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController,
    private sharedProvider: SharedProvider, private donationProvider: DonationProvider) {
      this.getAllDonations()
  }

  getAllDonations(){
    this.token = localStorage.getItem('token')
    this.data = {
      'campaignId': localStorage.getItem('campaign'),
      'productId': localStorage.getItem('product')
    };
    this.sharedProvider.showLoader()
    this.donationProvider.getDonations(this.token, this.data).then(result=>{
      this.sharedProvider.dismissLoader()
      this.donations = result
      this.tmpDonations = result
    }).catch(err=>{
      this.sharedProvider.dismissLoader()
      console.log(err)
    })
  }

  filterDonation(){
    let modal = this.modalCtrl.create(DonationFilterPage,{
      "donations": this.tmpDonations
    });
    modal.onDidDismiss(data => {
      this.donations = data
    });
    modal.present()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DonationPage');
  }

  donarDetail(donationId){
    let modal = this.modalCtrl.create(DonardetailPage,{
      "donationId": donationId
    })
    modal.present()
  }
}

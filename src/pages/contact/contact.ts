import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '../../../node_modules/@ionic-native/social-sharing';
import { CallNumber } from '../../../node_modules/@ionic-native/call-number';
import { THIS_EXPR } from '../../../node_modules/@angular/compiler/src/output/output_ast';

/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  public email: string

  constructor(public navCtrl: NavController, public navParams: NavParams, private socialSharing: SocialSharing,
  private callNum: CallNumber) {
    let campaignAttr = JSON.parse((localStorage.getItem('campaign_attr')));
    this.email = campaignAttr.email
    console.log(campaignAttr)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

  enquireViaMail(email){
    let subject = 'UnitedWay Mumbai Help'
    this.socialSharing.shareViaEmail('', subject, email)
  }

  enquireViaWhatsapp(mobileNo){
    this.socialSharing.shareViaWhatsAppToReceiver(mobileNo, '', '', '')
  }

  callNumber(mobileNo){
    this.callNum.callNumber(mobileNo, true).then(res=>{

    }).catch(err=>{
      
    })
  }

}

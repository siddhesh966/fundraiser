import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SuccessfullFundraiserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-successfull-fundraiser',
  templateUrl: 'successfull-fundraiser.html',
})
export class SuccessfullFundraiserPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  openSuccessfullFundraiserDoc(){
   // let url = 'https://docs.google.com/forms/d/e/1FAIpQLScb7XCm-65Xg7a9k2OV3SNaEJcIWJ7isgXPHlG7XybgLdZOtA/viewform?entry.1144931537&entry.1285065167'
      let url ='https://docs.google.com/forms/d/e/1FAIpQLSd5asDcKf71_rUenXMiOruvTMo40UlxQkGhYd0PHZDdWV8y8A/viewform';
   window.open(url, '_blank')
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SuccessfullFundraiserPage');
  }

}

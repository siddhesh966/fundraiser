import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { CampaignProvider } from '../../providers/campaign/campaign';
import { HomePage } from '../home/home';

/**
 * Generated class for the CampaignSelectorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-campaign-selector',
  templateUrl: 'campaign-selector.html',
})
export class CampaignSelectorPage {

  public campaigns: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public sharedProvider: SharedProvider, 
  public campaignProvider: CampaignProvider, public event: Events) {
    this.getAllCampaigns();
  }

  getAllCampaigns(){
    let token = localStorage.getItem('token')
    console.log(token)
    this.sharedProvider.showLoader()
    this.campaignProvider.getCampaigns(token).then(result=>{
      this.sharedProvider.dismissLoader()
      this.campaigns = result
    }).catch(err=>{
      this.sharedProvider.dismissLoader()
    })
  }

  selectCampaign(campaign_id, product_id, title, campaign_attr){
    localStorage.setItem('title', title)
    localStorage.setItem('campaign', campaign_id)
    localStorage.setItem('product', product_id)
    localStorage.setItem('campaign_attr', campaign_attr)
    this.event.publish('campaign:selected', [])
    this.navCtrl.setRoot(HomePage)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CampaignSelectorPage');
  }

}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '../../../node_modules/@angular/forms';
import { SharedProvider } from '../../providers/shared/shared';
import { NgoProvider } from '../../providers/ngo/ngo';
import { DonationProvider } from '../../providers/donation/donation';

@Component({
  selector: 'page-offlinedonation',
  templateUrl: 'offlinedonation.html',
})
export class OfflinedonationPage {

  offlineDonationForm: FormGroup
  token: any
  ngoLists: any
  response: any

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,
    private sharedProvider:SharedProvider,private ngoProvider: NgoProvider, private donationProvider: DonationProvider) {
    this.offlineDonationForm = this.formBuilder.group({
      donationFor: [''],
      productId: [''],
      campaignId: [''],
      ngoId: ['', Validators.required],
      amount: ['', Validators.compose([Validators.required,Validators.pattern('^[0-9]*$')])],
      anonymous: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')],
      address: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      country: [''],
      pincode: ['', Validators.compose([Validators.required,Validators.pattern('^[0-9]*$')])],
      telephone: [''],
      panNumber: [''],
      chequeNumber: ['', Validators.compose([Validators.required,Validators.pattern('^[0-9]*$')])],
      bankName: ['', Validators.required],
      chequeDate: ['', Validators.required],
      branchName: ['', Validators.required]
    })
    this.offlineDonationForm.reset()
    this.allNgoLists()
  }

  allNgoLists(){
    this.sharedProvider.showLoader()
    this.token = localStorage.getItem('token')
    let data = {
      'campaignId': localStorage.getItem('campaign'),
      'productId': localStorage.getItem('product')
    }
    this.ngoProvider.getUserNgo(this.token, data).then(result=>{
      this.sharedProvider.dismissLoader()
      this.ngoLists = result
    }).catch(err=>{
      this.sharedProvider.dismissLoader()
      console.log(err)
    })
  }

  doSave(){
    console.log(this.offlineDonationForm.value)
    this.token = localStorage.getItem('token')
    this.sharedProvider.showLoader()
    this.offlineDonationForm.controls['donationFor'].setValue(localStorage.getItem('title'))
      this.offlineDonationForm.controls['productId'].setValue(localStorage.getItem('product'))
      this.offlineDonationForm.controls['campaignId'].setValue(localStorage.getItem('campaign'))
    console.log(this.offlineDonationForm.value)
    this.donationProvider.saveOfflineDonation(this.token, this.offlineDonationForm.value).then(result=>{
      this.sharedProvider.dismissLoader()
      this.response = result
      this.offlineDonationForm.reset()
      this.sharedProvider.presentToast(this.response.message)
    }).catch(err=>{
      this.sharedProvider.dismissLoader()
      this.sharedProvider.presentToast("Something went wrong")
      console.log(err)
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfflinedonationPage');
  }

}

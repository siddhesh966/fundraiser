import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OfflinedonationPage } from '../offlinedonation/offlinedonation';

/**
 * Generated class for the PredonationFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-predonation-form',
  templateUrl: 'predonation-form.html',
})
export class PredonationFormPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  openDonationForm(){
    this.navCtrl.push(OfflinedonationPage)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PredonationFormPage');
  }

}

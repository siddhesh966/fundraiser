import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UpdatesPage } from '../updates/updates';

/**
 * Generated class for the PreUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-pre-update',
  templateUrl: 'pre-update.html',
})
export class PreUpdatePage {

  heading: any = 'Share an update on your campaign or thank your donors on your fundraiser page'
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  gotoUpdates(){
    this.navCtrl.push(UpdatesPage)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreUpdatePage');
  }

}

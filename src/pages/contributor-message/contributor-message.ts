import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SharedProvider } from '../../providers/shared/shared';
import { DonationProvider } from '../../providers/donation/donation';

/**
 * Generated class for the ContributorMessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-contributor-message',
  templateUrl: 'contributor-message.html',
})
export class ContributorMessagePage {

  contributorMessages: any
  response: any
  token: any

  constructor(public navCtrl: NavController, public navParams: NavParams,private sharedProvider: SharedProvider,
  private donationProvider: DonationProvider) {
    this.token = localStorage.getItem('token')
    this.getContributorMessages()
  }

  getContributorMessages(){
    this.sharedProvider.showLoader()
    let data = {
      'campaignId': localStorage.getItem('campaign'),
      'productId': localStorage.getItem('product')
    }
    this.donationProvider.contributorMessage(this.token, data).then(result=>{
      this.sharedProvider.dismissLoader()
      console.log(result)
      this.contributorMessages = result
    }).catch(err=>{
      this.sharedProvider.dismissLoader()
      this.sharedProvider.presentToast("Something went wrong!")
    })
  }

  changeStatus(messageId, status){
    let data = {
      'messageId': messageId,
      'status': status
    }
    console.log(data)
    this.sharedProvider.showLoader()
    this.donationProvider.updateContributorMessageStatus(this.token, data).then(result=>{
      this.sharedProvider.dismissLoader()
      this.sharedProvider.presentToast("Status updated successfully!")
      this.navCtrl.setRoot(this.navCtrl.getActive().component)
    }).catch(err=>{
      this.sharedProvider.dismissLoader()
      this.sharedProvider.presentToast("Something went wrong!")
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContributorMessagePage');
  }

}

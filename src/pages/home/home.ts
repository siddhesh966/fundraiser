import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, IonicSlides, ModalController } from '@ionic/angular';
import { SharedProvider } from '../../providers/shared/shared';
import { DonationProvider } from '../../providers/donation/donation';
import { UpdateProvider } from '../../providers/update/update';
import { UserProvider } from '../../providers/user/user';
import { SocialSharing } from '../../../node_modules/@ionic-native/social-sharing';
import { UpdatesPage } from '../updates/updates';
import { DonationPage } from '../donation/donation';
import { AnnouncementPage } from '../announcement/announcement';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(IonicSlides) slides: IonicSlides;

  public workoutProgress: string = '0' + '%';
  token: any
  latestUpdates: any
  recentDonations: any
  leaderBoardResp: any
  data: any
  userName: any
  userImage: any = null
  userId: any
  keys: any
  response: any
  isImagePresent: Boolean;
  message: any
  subject: any
  file: any
  url: any

  public achievement: any = {
    achieved: 0,
    target: 0,
    total: 0
  }

  constructor(public navCtrl: NavController, private menu:MenuController, private sharedProvider: SharedProvider,
    private donationProvider: DonationProvider, private updateProvider: UpdateProvider, private userProvider: UserProvider,
    private socialSharing: SocialSharing, private modalCtrl: ModalController) {
    this.menu.enable(true)
    this.data = {
      'campaignId': localStorage.getItem('campaign'),
      'projectId': localStorage.getItem('product')
    };
    this.userName = localStorage.getItem('fullName')
    this.userImage = localStorage.getItem('image')
    if(this.userImage == ''){
      this.isImagePresent = false
    }else{
      this.isImagePresent = true
    }
    this.userId = localStorage.getItem('userId')
    this.message = 'Please support my fundraising campaign '
    this.subject = ''
    this.file = ''
    let campaignAttr = JSON.parse((localStorage.getItem('campaign_attr')));
    this.url = 'http://www.unitedwaymumbai.org/'+campaignAttr.share_url+'-'+this.userId
    this.token = localStorage.getItem('token')
    this.dashboard()
  }

  dashboard(){
    console.log(this.userId)
    this.sharedProvider.showLoader()
    this.updateProvider.latestUpdates(this.token, this.data).then(result=>{this.latestUpdates = result})
    this.donationProvider.recentDonation(this.token, this.data).then(res=>{this.recentDonations=res})
    this.userProvider.leaderBoardPosition(this.token, this.data).then(res=>{
      this.response=res
      this.leaderBoardResp = this.response.datas
      console.log(this.leaderBoardResp)

      this.keys = Object.keys(this.leaderBoardResp)
      setTimeout(() => {
        this.slides.slideTo(this.response.position,200);
      }, 1000);
      for(let i=0;i<=this.leaderBoardResp.length;i++){
        if(this.leaderBoardResp[i].user_id==this.userId){
          console.log(this.leaderBoardResp[i])
          this.achievement=this.leaderBoardResp[i]
        }
      }
    })
    // this.donationProvider.achievements(this.token, this.data).then(res=>{
    //   this.achievement=res
    //   console.log(this.achievement)
    //   this.workoutProgress = this.achievement.percentage + '%'
    // })
    this.sharedProvider.dismissLoader()
  }

  slidePrev(){
    this.slides.slidePrev()
  }

  slideNext(){
    this.slides.slideNext()
  }

  whatsappShare(){
    console.log("Clicked whatsapp sharing...")
    this.socialSharing.shareViaWhatsApp(this.message, this.file, this.url)
  }

  facebookShare(){
    this.socialSharing.shareViaFacebook(this.message, this.file, this.url)
  }

  emailShare(){
    this.message = this.message +' '+this.url
    this.socialSharing.shareViaEmail(this.message, this.subject, [], [], [],this.file)
  }


  share(){
    this.socialSharing.share(this.message, this.subject, this.file, this.url)
  }

  showAllDonations(){
    this.navCtrl.push(DonationPage)
  }

  showAllUpdates(){
    this.navCtrl.push(UpdatesPage)
  }

  openAnnouncement(content){
    let modal = this.modalCtrl.create(AnnouncementPage,{
      'content': content
    })
    modal.present()
  }

}

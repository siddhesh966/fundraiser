import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TeamPage } from '../team/team';

/**
 * Generated class for the PreTeamFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-pre-team-form',
  templateUrl: 'pre-team-form.html',
})
export class PreTeamFormPage {

  heading: any = 'Many fundraisers choose to raise funds jointly with friends & family. With this feature, you would be able to upload their pictures so that they would also be featured on your fundraising page.'

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  gotoTeam(){
    this.navCtrl.push(TeamPage)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreTeamFormPage');
  }

}

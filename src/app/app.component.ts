import {Component, ViewChild} from '@angular/core';
import { IonNav,Platform, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { StatusBar } from '@capacitor/status-bar';
import { SplashScreen } from '@capacitor/splash-screen';
import { SharedProvider } from '../providers/shared/shared';
import { UserProvider } from '../providers/user/user';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ContactPage } from '../pages/contact/contact';
/*
import { AppealPage } from '../pages/appeal/appeal';
import { NgoPage } from '../pages/ngo/ngo';
import { DonationPage } from '../pages/donation/donation';
import { ProfilePage } from '../pages/profile/profile';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';
import { ContributorMessagePage } from '../pages/contributor-message/contributor-message';
import { Push, PushObject, PushOptions } from '../../node_modules/@ionic-native/push';
import { Badge } from '../../node_modules/@ionic-native/badge';
import { PredonationFormPage } from '../pages/predonation-form/predonation-form';
import { SuccessfullFundraiserPage } from '../pages/successfull-fundraiser/successfull-fundraiser';
import { PreUpdatePage } from '../pages/pre-update/pre-update';
import { PreTeamFormPage } from '../pages/pre-team-form/pre-team-form';
import { CampaignSelectorPage } from '../pages/campaign-selector/campaign-selector';
*/

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  @ViewChild(IonNav) nav: IonNav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any}>;
  user: any = {
    'image': '../assets/imgs/profile-pic.png',
    'fullName': 'Aditya',
    'email': 'aditya@technople.in'
  };
  token: any;
  isLoggedIn: any;
  tokenId: any;
  title: any = localStorage.getItem('token')

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
              private event: Events, public sharedProvider: SharedProvider,private userProvider: UserProvider,
              private push: Push,private alertCtrl: AlertController, private badge: Badge) {

    this.user = sharedProvider.getUserInfo()
    this.title = localStorage.getItem('title')
    console.log(this.user)
    this.initializeApp();
    this.event.subscribe('user:updated', () => {
      this.user = sharedProvider.getUserInfo()
    })
    this.event.subscribe('campaign:selected', () => {
      this.title = localStorage.getItem('title')
    })

    // used for an example of ngFor and navigation
    this.pages = [
      {title: 'Dashboard', component: HomePage},
      /*{title: 'My Appeal/Target', component: AppealPage},
      {title: 'My NGOs', component: NgoPage},
      {title: 'My Updates', component: PreUpdatePage},
      {title: 'My Team', component: PreTeamFormPage},
      {title: 'View Donations', component: DonationPage},
      {title: 'Add Offline Donation', component: PredonationFormPage},
      {title: 'View Contributor Messages', component: ContributorMessagePage},
      {title: 'Switch Campaign', component: CampaignSelectorPage},
      {title: 'Fundraising Tips', component: SuccessfullFundraiserPage},
      {title: 'My Account', component: ProfilePage},
      {title: 'Change Password', component: ChangepasswordPage},*/
      {title: 'Help', component: ContactPage}
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      //this.initPushNotification();
      this.configureBadge()
      this.isLoggedIn = this.sharedProvider.isLoggedIn();
      let selectedCampaign = localStorage.getItem('campaign')
      if(this.isLoggedIn){
        if(selectedCampaign!=null){
          this.nav.setRoot(HomePage);
        }else{
          //this.nav.setRoot(CampaignSelectorPage)
        }
      }
    });
  }

  configureBadge(){

  }
/*
  initPushNotification() {
    const options: PushOptions = {
      android: {
        senderID: '868596734441'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {},
      browser: {}
    };

    const pushObject: PushObject = this.push.init(options);


    pushObject.on('notification').subscribe((notification: any) => {
      console.log('message -> ' + notification.message);
      //if user using app and push notification comes
      if (notification.additionalData.foreground) {
        // if application open, show popup
        this.badge.increase(1)

        let confirmAlert = this.alertCtrl.create({
          title: notification.title,
          message: notification.message,
          buttons: [{
            text: 'Ignore',
            role: 'cancel'
          }, {
            text: 'View',
            handler: () => {
              //TODO: Your logic here
              this.nav.push(DonationPage);
            }
          }]
        });
        confirmAlert.present();
      } else {
        //if user NOT using app and push notification comes
        //TODO: Your logic on click of push notification directly
        this.nav.push(DonationPage);
        console.log('Push notification clicked');
      }
    });

    pushObject.on('registration').subscribe((registration: any) => {
      let fcmresp = registration
      localStorage.setItem('tokenId',fcmresp.registrationId);
    });

    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }*/

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  doLogout(){
    this.sharedProvider.showLoader()
    this.tokenId = localStorage.getItem('tokenId')
    this.token = localStorage.getItem('token')
    this.userProvider.logout(this.token).then(result=>{
      this.sharedProvider.clearLocalStorage()
      localStorage.setItem('tokenId', this.tokenId)
      this.sharedProvider.dismissLoader()
      this.nav.setRoot(LoginPage)
    }).catch(err=>{
      this.sharedProvider.dismissLoader()
      this.sharedProvider.presentToast("Something went wrong!")
    })
  }
}

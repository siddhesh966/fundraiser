import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UpdateProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UpdateProvider {

  apiUrl= 'https://www.unitedwaymumbai.org/app_test/index.php?modules=api&'
  
  constructor(public http: HttpClient) {
    console.log('Hello UpdateProvider Provider');
  }

  allUpdates(token, data){
    return new Promise((resolve, reject)=>{
      this.http.post(this.apiUrl+'controller=updates&action=myupdates',JSON.stringify(data),{
        headers: new HttpHeaders().set('Accept','application/json').set('Token', token)
      }).subscribe(res=>{
        resolve(res)
      },(err)=>{
        reject(err)
      })
    })  
  }

  deleteUpdate(token, data){
    return new Promise((resolve, reject)=>{
      this.http.post(this.apiUrl+'controller=updates&action=delete',JSON.stringify(data),{
        headers: new HttpHeaders().set('Accept','application/json').set('Token', token).set('Content-Type','application/json')
      }).subscribe(res=>{
        resolve(res)
      },(err)=>{
        reject(err)
      })
    })  
  }

  saveUpdate(token, data){
    return new Promise((resolve, reject)=>{
      this.http.post(this.apiUrl+'controller=updates&action=add',JSON.stringify(data),{
        headers: new HttpHeaders().set('Accept','application/json').set('Token', token).set('Content-Type','application/json')
      }).subscribe(res=>{
        resolve(res)
      },(err)=>{
        reject(err)
      })
    })
  }

  latestUpdates(token, data){
    return new Promise((resolve, reject)=>{
      this.http.post(this.apiUrl+'controller=general&action=announcements', JSON.stringify(data),{
        headers: new HttpHeaders().set('Accept','application/json').set('Token', token)
      }).subscribe(res=>{
        resolve(res)
      },(err)=>{
        reject(err)
      })
    })
  }
}

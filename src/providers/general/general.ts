import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the GeneralProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GeneralProvider {

  apiUrl= 'https://www.unitedwaymumbai.org/app_test/index.php?modules=api&'
  
  constructor(public http: HttpClient) {
    console.log('Hello GeneralProvider Provider');
  }

  getAllCountries(token){
    return new Promise((resolve, reject)=>{
      this.http.get(this.apiUrl+'controller=general&action=nationality',{
        headers: new HttpHeaders().set('Token',token).set('Accept','application/json')
      }).subscribe(res=>{
        resolve(res)
      },(err)=>{
        reject(err)
      })
    })
  }

}
